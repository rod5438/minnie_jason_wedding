import 'package:flutter/material.dart';
import 'package:minnie_jason_wedding/data/StoreData.dart';

class PhoneBookCellWidget extends StatelessWidget {
  const PhoneBookCellWidget({required this.data, Key? key}) : super(key: key);
  final StoreData data;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Colors.lightGreen))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(data.storeName, style: TextStyle(fontSize: 26)),
                Text(data.address),
                Text(data.webAddress, overflow: TextOverflow.ellipsis, maxLines: 1),
                Text(data.phoneNumber),
              ],
            ),
          ),
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.info_outlined),
          )
        ],
      ),
    );
  }
}