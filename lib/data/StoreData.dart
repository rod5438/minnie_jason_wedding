enum StoreType {
  dress, // 婚紗公司
  photo, // 攝影公司
  print, // 印刷公司
  restaurant, // 喜宴餐廳
}

extension StoreTypeExtension on StoreType {
  String get name {
    switch (this) {
      case StoreType.dress:
        return "dress";
      case StoreType.photo:
        return "photo";
      case StoreType.print:
        return "print";
      case StoreType.restaurant:
        return "restaurant";
    }
  }
}

extension StringExtension on String {
  StoreType get storeType {
    switch (this) {
      case "dress":
        return StoreType.dress;
      case "photo":
        return StoreType.photo;
      case "print":
        return StoreType.print;
      case "restaurant":
        return StoreType.restaurant;
      default:
        throw Error();
    }
  }
}

List<StoreData> buildInData = [
  {
    "storeName": "亞太麗緻婚紗",
    "phoneNumber": "02-2761-6727",
    "webAddress": "http://apiwi.com/",
    "address": "105台北市松山區八德路四段666號",
    "type": "dress",
    "isFavorites": "0"
  },
  {
    "storeName": "CH WEDDING經典婚紗",
    "phoneNumber": "02-2702-7685",
    "webAddress": "http://www.c-hwedding.com/",
    "address": "106台北市大安區安和路一段133號",
    "type": "dress",
    "isFavorites": "0"
  },
  {
    "storeName": "Hanwei Huang",
    "phoneNumber": "0972-335-749",
    "webAddress": "https://www.facebook.com/slashlovepa3",
    "address": "",
    "type": "photo",
    "isFavorites": "0"
  },
  {
    "storeName": "Pure Bridal Makeup Studio",
    "phoneNumber": "02-2702-7685",
    "webAddress": "https://www.facebook.com/ashleystudio.kr",
    "address": "fanta420@hotmail.com",
    "type": "photo",
    "isFavorites": "0"
  },
  {
    "storeName": "健豪印刷",
    "phoneNumber": "02-2760-3586",
    "webAddress": "http://www.gding.com.tw/",
    "address": "105台北市松山區八德路四段78號",
    "type": "print",
    "isFavorites": "0"
  },
  {
    "storeName": "Andwedding",
    "phoneNumber": "04-23129192",
    "webAddress": "http://www.andwedding.com.tw/",
    "address": "台中市西屯區四川路78巷5號",
    "type": "print",
    "isFavorites": "0"
  },
  {
    "storeName": "儷宴會館",
    "phoneNumber": "02-2536-8899",
    "webAddress": "http://www.tgarden.com.tw/",
    "address": "104台北市中山區林森北路413號",
    "type": "restaurant",
    "isFavorites": "0"
  },
  {
    "storeName": "一家餐廳",
    "phoneNumber": "08-932-9696",
    "webAddress": "http://tour.taitung.gov.tw/zh-tw/Dining/Shop/8/%E4%B8%80%E5%AE%B6%E9%A4%90%E5%BB%B3",
    "address": "950台東縣台東市更生路321號",
    "type": "restaurant",
    "isFavorites": "0"
  },
].map((e) => StoreData.fromJson(e)).toList();

class StoreData {
  StoreData.fromJson(this.json);

  String get storeName => json["storeName"];

  String get phoneNumber => json["phoneNumber"];

  String get address => json["address"];

  String get webAddress => json["webAddress"];

  StoreType get type => json["type"].toString().storeType;

  bool get isFavorites => json["isFavorites"];

  String get storeID => json["storeID"];
  Map json;
}
