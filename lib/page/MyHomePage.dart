import 'package:flutter/material.dart';
import 'package:minnie_jason_wedding/data/StoreData.dart';
import 'package:minnie_jason_wedding/page/PhoneBookPage.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class HomePageObject {
  final String buttonImg;
  final String bookName;
  final StoreType type;

  HomePageObject({required this.buttonImg, required this.bookName, required this.type});
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: GridView.count(
          crossAxisCount: 2,
          mainAxisSpacing: 10,
          children: [
            HomePageObject(buttonImg: "assets/images/bookYellow.png", bookName: "婚紗", type: StoreType.dress),
            HomePageObject(buttonImg: "assets/images/bookGreen.png", bookName: "攝影", type: StoreType.photo),
            HomePageObject(buttonImg: "assets/images/bookBlue.png", bookName: "喜宴", type: StoreType.restaurant),
            HomePageObject(buttonImg: "assets/images/bookRed.png", bookName: "喜帖", type: StoreType.print),
          ]
              .map((e) => Center(
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PhoneBookPage(title: e.bookName, type: e.type,)),
                        );
                      },
                      child: Stack(
                        children: [
                          Image.asset(e.buttonImg),
                          Center(
                            child: Text(
                              e.bookName,
                              style: TextStyle(fontSize: 20),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ))
              .toList(),
        ),
      ),
    );
  }
}
