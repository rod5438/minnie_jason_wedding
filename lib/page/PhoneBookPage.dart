import 'package:flutter/material.dart';
import 'package:minnie_jason_wedding/data/StoreData.dart';
import 'package:minnie_jason_wedding/widget/PhoneBookCellWidget.dart';

class PhoneBookPage extends StatefulWidget {
  PhoneBookPage({required this.title, required this.type, Key? key}) : super(key: key);
  final String title;
  final StoreType type;

  @override
  _PhoneBookPageState createState() => _PhoneBookPageState();
}

class _PhoneBookPageState extends State<PhoneBookPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView(
        children: buildInData
            .where((element) => element.type == widget.type)
            .map((e) => PhoneBookCellWidget(data: e))
            .toList(),
      ),
    );
  }
}
